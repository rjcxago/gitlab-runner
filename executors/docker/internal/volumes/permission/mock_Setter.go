// Code generated by mockery v2.14.0. DO NOT EDIT.

package permission

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
)

// MockSetter is an autogenerated mock type for the Setter type
type MockSetter struct {
	mock.Mock
}

// Set provides a mock function with given fields: ctx, volumeName, labels
func (_m *MockSetter) Set(ctx context.Context, volumeName string, labels map[string]string) error {
	ret := _m.Called(ctx, volumeName, labels)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, map[string]string) error); ok {
		r0 = rf(ctx, volumeName, labels)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

type mockConstructorTestingTNewMockSetter interface {
	mock.TestingT
	Cleanup(func())
}

// NewMockSetter creates a new instance of MockSetter. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewMockSetter(t mockConstructorTestingTNewMockSetter) *MockSetter {
	mock := &MockSetter{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
